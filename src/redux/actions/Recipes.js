import { UPDATE_RECIPE, UPDATE_RECIPES, DELETE_RECIPE } from '../../@jumbo/constants/ActionTypes';

export const updateRecipe = objectRecipe => { 
  console.log('IN ACTIONS',objectRecipe)

  return dispatch => {
    dispatch({
      type: UPDATE_RECIPE,
      payload: objectRecipe,
    });
  };
};

export const updateRecipes = recipes => {
  return dispatch => {
    dispatch({
      type: UPDATE_RECIPES,
      payload: recipes,
    });
  };
};

export const deleteRecipe = id => { 
  return dispatch => {
    dispatch({
      type: DELETE_RECIPE,
      payload: id,
    });
  };
};
