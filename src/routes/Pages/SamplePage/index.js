import React from 'react';
import GridContainer from '../../../@jumbo/components/GridContainer';
import PageContainer from '../../../@jumbo/components/PageComponents/layouts/PageContainer';
import Box from '@material-ui/core/Box';
import IntlMessages from '../../../@jumbo/utils/IntlMessages';
import Grid from '@material-ui/core/Grid';
import VisibilityIcon from '@material-ui/icons/Visibility';
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";

const breadcrumbs = [
  { label: 'Home', link: '/' },
  { label: 'Evaluations', isActive: true },
];

const useStyles = makeStyles({
  table: {
    minWidth: 650
  }
});

function createData(date, title, score) {
  return { date, title, score };
}

const rows = [
  createData("10-03-1989", 'Behavior evaluation', '5'),
  createData("10-03-1989", 'Behavior evaluation', '5'),
  createData("10-03-1989", 'Behavior evaluation', '5'),
  createData("10-03-1989", 'Behavior evaluation', '5'),
  createData("10-03-1989", 'Behavior evaluation', '5')
];



const Index = () => {
  const classes = useStyles();


  return (
    <PageContainer heading={<IntlMessages id="pages.evaluations" />} breadcrumbs={breadcrumbs}>
    </PageContainer>
  );
};

export default Index;
