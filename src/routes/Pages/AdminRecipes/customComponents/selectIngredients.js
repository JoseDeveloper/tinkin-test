import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import Chip from '@material-ui/core/Chip';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';

export default function SelectIngredients({ handleSelectIngredients, selectedIngredients }) {
    const { ingredients: { ingredients } } = useSelector(({ recipes }) => recipes);
    const fixedOptions = [];
    const [value, setValue] = React.useState([]);

    useEffect(() => {
        setValue(selectedIngredients);

    }, []);

    return (
        <Autocomplete
            multiple
            id="selectIngredients"
            value={value}
            disableCloseOnSelect
            onChange={(event, newValue) => {
                setValue([
                    ...fixedOptions,
                    ...newValue.filter((option) => fixedOptions.indexOf(option) === -1),
                ]);

                handleSelectIngredients(newValue);
            }}
            options={ingredients}
            getOptionLabel={(option) => {
                let label = option.name.label;
                label = label.replace("_", " ");
                label = label.replace(/[^0-9A-Za-z ,]/g, '');
                label = label.toLowerCase();
                return label.charAt(0).toUpperCase() + label.slice(1)
            }}
            renderTags={(tagValue, getTagProps) =>
                tagValue.map((option, index) => {
                    let label = option.name.label;
                    label = label.replace("_", " ");
                    label = label.replace(/[^0-9A-Za-z ,]/g, '');
                    label = label.toLowerCase();
                    label = label.charAt(0).toUpperCase() + label.slice(1)
                    return (
                        <Chip
                            label={label}
                            {...getTagProps({ index })}
                            disabled={fixedOptions.indexOf(option) !== -1}
                        />
                    )
                })}

            style={{ width: 500 }}
            renderInput={(params) => (
                <TextField {...params} label="Select ingredients" variant="outlined" placeholder="Favorites" />
            )}
        />
    );
}
