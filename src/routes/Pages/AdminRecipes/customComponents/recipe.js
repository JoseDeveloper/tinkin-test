import React from 'react';
import { useDispatch } from 'react-redux';
import CmtCard from '../../../../@coremat/CmtCard';
import { Box } from '@material-ui/core';
import CmtImage from '../../../../@coremat/CmtImage';
import Button from '@material-ui/core/Button';
import CmtCardContent from '../../../../@coremat/CmtCard/CmtCardContent';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Typography from '@material-ui/core/Typography';
import DeleteIcon from '@material-ui/icons/Delete';

import { deleteRecipe } from '../../../../redux/actions/Recipes';

const images = [
    'https://i1.wp.com/vegecravings.com/wp-content/uploads/2017/01/Fruit-Custard-Recipe-Step-By-Step-Instructions.jpg?fit=2152%2C1748&quality=65&strip=all&ssl=1',
    'https://dinnerthendessert.com/wp-content/uploads/2019/03/Fruit-Salad-3.jpg',
    'https://www.spendwithpennies.com/wp-content/uploads/2019/04/Fruit-Salad-SWP.jpg',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSB9joLUouJUPhNjV3cTUokbK_G5c3_Sed4mg&usqp=CAU',
    'https://images-gmi-pmc.edge-generalmills.com/023f25df-408a-44fc-b74e-be5b1dcd4bdf.jpg'
];

const useStyles = makeStyles(() => ({
    cardRoot: {
        height: '100%',
        padding: '50px',
        '& .Cmt-card-content': {
            height: '100%',
            display: 'flex',
            flexDirection: 'column',
        },
    },
    btnRoot: {
        height: 40,
    },
    img: {
        width: '150px',
        height: '150px'
    }
}));


const Recipe = ({ data, openModal, handleOpenDialogView }) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const { name, description, nImage, id } = data;

    const deleteItem = (e, id) => { console.log('ID',id)
        e.preventDefault();
        dispatch(deleteRecipe(id));

    }
    return (
        <CmtCard className={classes.cardRoot}>
            <CmtCardContent>
                <Box display="flex" alignItems="center" mb={5}>
                    <Box mr={{ xs: 4, xl: 6 }}>
                        <CmtImage src={images[nImage]} alt="flying" className={classes.img} />
                    </Box>
                    <Box flex={1}>
                        <Typography component="div" variant="h2">
                            {name}
                        </Typography>
                        <Box mt={1} component="p" color="text.secondary">
                            {name}
                        </Box>
                    </Box>
                </Box>
                <Box mt="auto">
                    <Button variant="outlined" color="primary" className={classes.btnRoot} htmltype="submit" onClick={(e) => openModal(e, data)}>
                        Edit
                    </Button>
                    <Button variant="outlined" color="primary" className={classes.btnRoot} htmltype="submit" onClick={(e) => handleOpenDialogView(e, data)}>
                        Ver
                    </Button>
                    <a href="#" onClick={(e) => deleteItem(e, id)}><DeleteIcon color="secondary" /></a>

                </Box>
            </CmtCardContent>
        </CmtCard>
    );
};

export default Recipe;