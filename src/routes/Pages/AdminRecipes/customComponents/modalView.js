import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { makeStyles } from "@material-ui/core/styles";
import Button from '@material-ui/core/Button';
import { Box } from '@material-ui/core';
import CmtImage from '../../../../@coremat/CmtImage';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import SelectIngredients from './selectIngredients';
import StarsRating from './starsRating';

const images = [
    'https://i1.wp.com/vegecravings.com/wp-content/uploads/2017/01/Fruit-Custard-Recipe-Step-By-Step-Instructions.jpg?fit=2152%2C1748&quality=65&strip=all&ssl=1',
    'https://dinnerthendessert.com/wp-content/uploads/2019/03/Fruit-Salad-3.jpg',
    'https://www.spendwithpennies.com/wp-content/uploads/2019/04/Fruit-Salad-SWP.jpg',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSB9joLUouJUPhNjV3cTUokbK_G5c3_Sed4mg&usqp=CAU',
    'https://images-gmi-pmc.edge-generalmills.com/023f25df-408a-44fc-b74e-be5b1dcd4bdf.jpg'
];


const useStyles = makeStyles({
    width: {
        width: '50%'
    },
    fullWidth: {
        padding: '10px',
        width: '80%'
    },
    padding: {
        padding: '10px'
    },
    img: {
        width: '150px',
        height: '150px'
    }
});

export default function ModalView({ openDialog, dialogData, closeDialog }) {
    const classes = useStyles();
    const { ingredients: { ingredients } } = useSelector(({ recipes }) => recipes);

    const [open, setOpen] = React.useState(false);
    const [data, setData] = React.useState({});
    const [selectedIngredients, setSelectedIngredients] = React.useState([]);
    const [name, handleName] = React.useState('');
    const [rating, handleRating] = React.useState(0);
    const [description, handleDescription] = React.useState('');
    const [instructions, handleInstructions] = React.useState('');
    const [nnImage, handleNImage] = React.useState(0);


    useEffect(() => {
        cleanState();
        setOpen(openDialog);
        setData(dialogData);
        const { name, rating, description, instructions, ingredients, nImage } = dialogData; console.log('DIALOG DATA', dialogData)
        handleName(name); handleRating(rating); handleDescription(description); handleInstructions(instructions); setSelectedIngredients(ingredients); handleNImage(nImage);


    }, [openDialog, dialogData]);

    const cleanState = () => {
        handleName(''); handleDescription(''); handleInstructions(''); setSelectedIngredients([]); handleRating(0);
    }

    const handleClose = () => {
        setData({});
        setOpen(false);
        closeDialog();
    };

    const handleSetStarsRating = () => {

    }

    return (
        <div>
            <Dialog
                open={open}
                onClose={handleClose}
                fullWidth={true}
                maxWidth={'md'}
                aria-labelledby="form-dialog-title"
            >
                <DialogTitle id="form-dialog-title">Recipe {data.name}</DialogTitle>


                <DialogContent>
                    <FormControl className={classes.width} >
                        <Box mr={{ xs: 4, xl: 6 }}>
                            <CmtImage src={images[nnImage]} alt="flying" className={classes.img} />
                        </Box>
                    </FormControl>
                    <FormControl className={classes.width} >
                        <StarsRating setStarsRating={handleSetStarsRating} rating={data.rating} />
                    </FormControl>

                    <FormControl className={classes.fullWidth} >
                        <span>Description</span>
                        <span>{description}</span>
                    </FormControl>
                    <FormControl className={classes.fullWidth} >
                        <span>Instructions</span>
                        <span>{instructions}</span>
                    </FormControl>

                    <FormControl className={classes.fullWidth} >
                        <span>Ingredients</span>
                        {selectedIngredients &&
                            selectedIngredients.map((item, i) => {
                                let label = item.name.label;
                                label = label.replace("_", " ");
                                label = label.replace(/[^0-9A-Za-z ,]/g, '');
                                label = label.toLowerCase();
                                label = label.charAt(0).toUpperCase() + label.slice(1)
                                return <span> - {label}</span>
                            })
                        }

                    </FormControl>

                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
          </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}