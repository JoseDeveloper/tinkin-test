import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { makeStyles } from "@material-ui/core/styles";
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import SelectIngredients from './selectIngredients';

// Redux Actions
import { updateRecipes } from '../../../../redux/actions/Recipes';

import StarsRating from './starsRating';

const useStyles = makeStyles({
    width: {
        width: '50%'
    },
    fullWidth: {
        padding: '10px',
        width: '100%'
    },
    padding: {
        padding: '10px'
    }
});


export default function FormDialogCreate({ openDialog, closeDialog }) {
    const classes = useStyles();
    const dispatch = useDispatch();
    const { ingredients: { ingredients }, recipes } = useSelector(({ recipes }) => recipes);

    const [open, setOpen] = React.useState(false);
    const [data, setData] = React.useState({});
    const [selectedIngredients, setSelectedIngredients] = React.useState([]);
    const [name, handleName] = React.useState('');
    const [rating, handleRating] = React.useState(0);
    const [description, handleDescription] = React.useState('');
    const [instructions, handleInstructions] = React.useState('');

    useEffect(() => {
        cleanState();
        setOpen(openDialog);

    }, [openDialog]);

    const cleanState = () => {
        handleName(''); handleDescription(''); handleInstructions(''); setSelectedIngredients([]); handleRating(0);
    }

    const handleClose = () => {
        setData({});
        setOpen(false);
        closeDialog();
    };

    const handleSelectIngredients = (values) => {
        setSelectedIngredients(values)

    }

    const handleSetStarsRating = (value) => {
        handleRating(value)

    }

    const save = async () => {
        const id = recipes.length + 1;
        let nImage = Math.floor(Math.random() * (4 - 0 + 1) + 0);
        const objectToSave = { id, name, description, rating, instructions, ingredients: selectedIngredients, nImage };

        dispatch(updateRecipes(objectToSave));
        closeDialog();
    }

    return (
        <div>
            <Dialog
                open={open}
                onClose={handleClose}
                fullWidth={true}
                maxWidth={'md'}
                aria-labelledby="form-dialog-title"
            >
                <DialogTitle id="form-dialog-title">Recipe {data.name}</DialogTitle>

                <DialogContent>
                    <FormControl className={classes.width} >
                        <TextField
                            value={name}
                            autoFocus
                            margin="dense"
                            label="Name"
                            type="text"
                            className={classes.width}
                            fullWidth
                            onChange={(e) => handleName(e.target.value)}
                        />
                    </FormControl>
                    <FormControl className={classes.width} >
                        <StarsRating setStarsRating={handleSetStarsRating} rating={data.rating} />
                    </FormControl>

                    <FormControl className={classes.width} >
                        <TextField
                            variant="outlined"
                            value={description}
                            autoFocus
                            margin="dense"
                            label="Description"
                            type="text"
                            className={classes.fullWidth}
                            fullWidth
                            multiline
                            rows={8}
                            rowsMax={12}
                            onChange={(e) => handleDescription(e.target.value)}
                        />
                    </FormControl>
                    <FormControl className={classes.width} >
                        <TextField
                            variant="outlined"
                            value={instructions}
                            autoFocus
                            margin="dense"
                            label="Instructions"
                            type="text"
                            className={classes.fullWidth}
                            fullWidth
                            multiline
                            rows={8}
                            rowsMax={12}
                            onChange={(e) => handleInstructions(e.target.value)}
                        />
                    </FormControl>

                    <FormControl className={classes.fullWidth} >
                        <SelectIngredients handleSelectIngredients={handleSelectIngredients} selectedIngredients={selectedIngredients} />
                    </FormControl>

                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
          </Button>
                    <Button onClick={save} color="primary">
                        Save
          </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}