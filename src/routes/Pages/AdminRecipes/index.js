import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import GridContainer from '../../../@jumbo/components/GridContainer';
import PageContainer from '../../../@jumbo/components/PageComponents/layouts/PageContainer';
import Box from '@material-ui/core/Box';
import IntlMessages from '../../../@jumbo/utils/IntlMessages';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

// Individual recipe component 
import Recipe from "./customComponents/recipe";

// Dialog.
import FormDialog from "./customComponents/modal";
import FormDialogCreate from "./customComponents/modalCreate";
import ModalView from "./customComponents/modalView";

const breadcrumbs = [
  { label: 'Home', link: '/' },
  { label: 'Recipes', isActive: true },
];

const useStyles = makeStyles({
  table: {
    minWidth: 650
  }
});


const Recipes = () => {
  const classes = useStyles();
  const { authUser } = useSelector(({ auth }) => auth);
  const { recipes } = useSelector(({ recipes }) => recipes);
  const dispatch = useDispatch();

  const [recipesData, setRecipes] = useState([]);
  const [openDialog, setOpenDialog] = useState(false);
  const [openDialogCreate, setOpenDialogCreate] = useState(false);
  const [openDialogView, setOpenDialogView] = useState(false);
  
  const [dialogData, setDialogData] = useState({});

  useEffect(() => {

    async function getRecipes() {
      setRecipes(recipes);
    }

    getRecipes();
  }, []);

  const handleOpenDialog = (e, rowData) => {
    e.preventDefault();
    setOpenDialog(true);
    setDialogData(rowData);
  };

  const handleOpenDialogCreate = () => {
    setOpenDialogCreate(true)
  }

  const handleCloseDialog = () => {
    setOpenDialog(false);
    setDialogData({});
  };

  const handleCloseDialogCreate = () => {
    setOpenDialogCreate(false);
  };

  const handleCloseDialogView = () => {
    setOpenDialogView(false);
  }

  const handleOpenDialogView = (e, rowData) => {
    setDialogData(rowData);
    setOpenDialogView(true);
  }


  return (
    <>
      <PageContainer heading={<IntlMessages id="pages.recipes" />} breadcrumbs={breadcrumbs}>
        <GridContainer>
          <Grid item xs={12}>
            <Box>
              <IntlMessages id="pages.recipes" />
            </Box>
          </Grid>
          <Grid item xs={12}>
            <Box>
              <Button onClick={handleOpenDialogCreate} color="primary">
                Create recipe
              </Button>
            </Box>
          </Grid>
          {recipesData &&
            recipesData.map((item, key) => {
              return <> <Grid item xs={6}> <Recipe data={item} openModal={handleOpenDialog} handleOpenDialogView={handleOpenDialogView}/> </Grid></>
            })
          }

        </GridContainer>

      </PageContainer>
      <FormDialog
        openDialog={openDialog}
        dialogData={dialogData}
        closeDialog={handleCloseDialog}
      />

      <FormDialogCreate
        openDialog={openDialogCreate}
        closeDialog={handleCloseDialogCreate}
      />

      <ModalView
        openDialog={openDialogView}
        dialogData={dialogData}
        closeDialog={handleCloseDialogView}
      />


    </>
  );
};

export default Recipes;
