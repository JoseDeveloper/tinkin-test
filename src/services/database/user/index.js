import { fetchError, fetchStart, fetchSuccess } from '../../../redux/actions';
import { setAuthUser, updateLoadUser } from '../../../redux/actions/Auth';
import React from 'react';
import axios from '../../config';

export const users = {
  getUsers: () => {

    const token = localStorage.getItem('token');
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    
    const response = axios.get('user/getUsers', {
      // user_id: userId
    })
      .then(({ data }) => {
        if (data) {
          
          return data;
        } else {
          return data.error;
        }
      })
      .catch(function (error) {
        return error;
      });
    return response;
  }
};
