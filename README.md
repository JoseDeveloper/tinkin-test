# Tinkin test

Test for Tinkin company

Paso 1.
Dirijase a la consola de su ID de desarrollo y ejecute el comando "yarn install".

Paso 2.
Para iniciar el proyecto ejecute el comando "yarn run start".

Paso 3.
Iniciar sesión con la cuenta por default superadmin@gmail.com clave: abcd.

Tecnologías usadas:
React js y Redux para manejo del estado general de la aplicación y base de datos local.

Rutas y archivos importantes para entender el flujo. 

1._ src\routes\Pages\AdminRecipes
Carpeta contenedora del archivo index, en el cual se ejecuta la carga de los componentes principales.

2._ src\routes\Pages\AdminRecipes\customComponents
Carpeta contenedora de todos los componentes a utilizar en el CRUD Admin Recipes.

3._ src\redux\actions\Recipes.js
Contiene los metodos actions a ejecutar en la báse de datos local administrada mediante Redux.

4._ src\redux\reducers\Recipes.js
Contiene los metodos reducers a ejecutar para nuestro entorno Redux.
